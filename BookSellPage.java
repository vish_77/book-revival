package book_revival;
import java.io.File;
import java.io.IOException;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Modality;
import javafx.stage.StageStyle;
import javafx.scene.layout.Pane;

public class BookSellPage extends Application {
   
    private User user;
    
    public void setUser(User user) {
        this.user = user;
    }
    Book book = new Book();

    @Override
    public void start(Stage primaryStage) {
         
        AnchorPane root = new AnchorPane();
        root.setPrefSize(1400, 700);
    
        root.getStyleClass().add("bgColor");
        root.getStylesheets().add("book_revival/SellPage.css");

        TextField textbookName = new TextField();
        textbookName.setFocusTraversable(false);
        textbookName.setLayoutX(594.0);
        textbookName.setLayoutY(125.0);
        textbookName.setPrefHeight(50.0);
        textbookName.setPrefWidth(500.0);
        textbookName.setPromptText("Name of TextBook");
        textbookName.setFont(new Font("Arial Black", 22.0));
        
        TextField publicationName = new TextField();
        publicationName.setFocusTraversable(false);
        publicationName.setLayoutX(594.0);
        publicationName.setLayoutY(371.0);
        publicationName.setPrefHeight(50.0);
        publicationName.setPrefWidth(500.0);
        publicationName.setPromptText("Publication");
        publicationName.setFont(new Font("Arial Black", 22.0));

        TextField branch = new TextField();
        branch.setFocusTraversable(false);
        branch.setLayoutX(595.0);
        branch.setLayoutY(206.0);
        branch.setPrefHeight(50.0);
        branch.setPrefWidth(500.0);
        branch.setPromptText("Branch");
        branch.setFont(new Font("Arial Black", 22.0));

        TextField price = new TextField();
        price.setFocusTraversable(false);
        price.setLayoutX(595.0);
        price.setLayoutY(530.0);
        price.setPrefHeight(50.0);
        price.setPrefWidth(500.0);
        price.setPromptText("Price");
        price.setFont(new Font("Arial Black", 22.0));

        TextField semester = new TextField();
        semester.setFocusTraversable(false);
        semester.setLayoutX(595.0);
        semester.setLayoutY(452.0);
        semester.setPrefHeight(50.0);
        semester.setPrefWidth(500.0);
        semester.setPromptText("Semester");
        semester.setFont(new Font("Arial Black", 22.0));

        TextField edition = new TextField();
        edition.setFocusTraversable(false);
        edition.setLayoutX(595.0);
        edition.setLayoutY(288.0);
        edition.setPrefHeight(50.0);
        edition.setPrefWidth(500.0);
        edition.setPromptText("Edition");
        edition.setFont(new Font("Arial Black", 22.0));
       

        ImageView bookUpload = new ImageView();
        bookUpload.setFocusTraversable(false);
        bookUpload.setFitHeight(256.0);
        bookUpload.setFitWidth(256.0);
        bookUpload.setLayoutX(132.0);
        bookUpload.setLayoutY(167.0);
        bookUpload.setPreserveRatio(true);
        bookUpload.setImage(new Image("File:book_revival/Photos/icons8-book-64.png"));



        bookUpload.setOnMouseClicked(new EventHandler<MouseEvent>() {

            private FileChooser fileChooser = new FileChooser();
            @Override
            public void handle(MouseEvent event) {
                System.out.println("ImageView (imageIcon) clicked!");
                

                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.jpeg", "*.gif"));

                File selectedFile = fileChooser.showOpenDialog(primaryStage);
                if (selectedFile != null) {
                    book.setBook_Image(selectedFile);
                    Image selectedImage = new Image(selectedFile.toURI().toString());
                    bookUpload.setImage(selectedImage);
                }
            }
        });

        Button submitButton = new Button("Submit");
        submitButton.setLayoutX(954);
        submitButton.setLayoutY(606);
        submitButton.setPrefWidth(140);
        submitButton.setPrefHeight(28);
        submitButton.setFont(Font.font("Arial Black", 24));

        submitButton.setOnAction(e -> {
            if (textbookName.getText().isEmpty() || edition.getText().isEmpty() || 
                publicationName.getText().isEmpty() || semester.getText().isEmpty() || 
                price.getText().isEmpty() || book.getBook_Image() == null) {
                System.out.println("Please fill in all required fields and select an image.");
            } else {
                book.setBookName(textbookName.getText());
                book.setBranch(branch.getText());
                book.setEdition(edition.getText());
                book.setPublication(publicationName.getText());
                book.setSemister(semester.getText());
                book.setPrice(price.getText());
                DBHandler dbHandler = new DBHandler();
                try {
                    dbHandler.submitButtonAction(book, user);
                    showPopupPane(); 
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        
        Button backButton = new Button("Back");
        backButton.setLayoutX(47);
        backButton.setLayoutY(606);
        backButton.setPrefWidth(150);
        backButton.setPrefHeight(20);
        backButton.setFont(Font.font("Arial Black", 24));
        
        backButton.setOnAction(e->{
            MainPage Main = new MainPage();
            Main.setUser(user);
            Main.start(primaryStage);
        });


        Label label = new Label("Upload TextBook Image");
        label.setFocusTraversable(false);
        label.setLayoutX(78.0);
        label.setLayoutY(456.0);
        label.setPrefHeight(31.0);
        label.setPrefWidth(367.0);
        label.setFont(new Font("Arial Black", 27.0));

        root.getChildren().addAll(textbookName, price, semester, edition, bookUpload, label, submitButton,backButton ,publicationName , branch);

        Scene scene = new Scene(root, 1400, 700);
        primaryStage.setScene(scene);
        primaryStage.show();
    }   

        private void showPopupPane() {
        Stage popupStage = new Stage();
        popupStage.initModality(Modality.APPLICATION_MODAL);
        popupStage.initStyle(StageStyle.UTILITY);
        popupStage.setTitle(" ");

        Pane popupPane = new Pane();
        popupPane.setPrefWidth(400);
        popupPane.setPrefHeight(200);
        ImageView imageView = new ImageView(new Image("file:book_revival/Photos/check-green.gif"));
        imageView.setFitWidth(100); 
        imageView.setFitHeight(100); 
        imageView.setLayoutX(50); 
        imageView.setLayoutY(50); 
        popupPane.getChildren().add(imageView); 


        Label label = new Label("Successfully Submitted !");
        label.setFont(Font.font("Arial Black", 15)); 
        label.setLayoutX(180); 
        label.setLayoutY(80); 
        popupPane.getChildren().add(label); 


        Scene popupScene = new Scene(popupPane);
        popupStage.setScene(popupScene);
        popupStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }
}