
package book_revival;
import java.io.File;
import java.io.IOException;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.Modality;
import javafx.stage.StageStyle;
import javafx.scene.layout.Pane;

public class SignupApplication extends Application {
    
    public TextField userButton;
    public PasswordField passwordButton;
    public RadioButton maleButton;
    public RadioButton femaleButton;
    public RadioButton otherButton;
    public TextField locationButton;
    public TextField contactButton;

    private Stage primaryStage;
    User user = new User();

    @Override
    public void start(Stage primaryStage) {

        this.primaryStage = primaryStage;

        AnchorPane root = new AnchorPane();
        root.setPrefSize(1400, 700);

        ImageView background = new ImageView(new Image("file:book_revival/Photos/wp12630165-book-pc-wallpapers.jpg"));
        background.setFitWidth(1417);
        background.setFitHeight(796);
        background.setLayoutX(-5);
        background.setLayoutY(-87);

        Text title = new Text("Create an Account");
        title.setFont(Font.font("Rockwell", 53));
        title.setLayoutX(190);
        title.setLayoutY(77);

        Label lab = new Label("Upload Image");
        lab.setFont(Font.font("Arial Black", 14));
        lab.setLayoutX(943);
        lab.setLayoutY(157);

        ImageView userIcon = new ImageView(new Image("file:book_revival/Photos/icons8-inquiry-skin-type-7-96.png"));

        userIcon.setFitWidth(71);
        userIcon.setFitHeight(59);
        userIcon.setLayoutX(645);
        userIcon.setLayoutY(196);

        ImageView passwordIcon = new ImageView(new Image("file:book_revival/Photos/icons8-password-96.png"));
        passwordIcon.setFitWidth(59);
        passwordIcon.setFitHeight(60);
        passwordIcon.setLayoutX(646);
        passwordIcon.setLayoutY(285);

        ImageView genderIcon = new ImageView(new Image("file:book_revival/Photos/icons8-gender-96.png"));
        genderIcon.setFitWidth(59);
        genderIcon.setFitHeight(60);
        genderIcon.setLayoutX(644);
        genderIcon.setLayoutY(367);

        ImageView locationIcon = new ImageView(new Image("file:book_revival/Photos/icons8-location-96.png"));
        locationIcon.setFitWidth(59);
        locationIcon.setFitHeight(60);
        locationIcon.setLayoutX(643);
        locationIcon.setLayoutY(441);

        ImageView callIcon = new ImageView(new Image("file:book_revival/Photos/icons8-call-96.png"));
        callIcon.setFitWidth(59);
        callIcon.setFitHeight(60);
        callIcon.setLayoutX(644);
        callIcon.setLayoutY(530);

        ImageView imageIcon = new ImageView(new Image("file:book_revival/Photos/icons8-add-user-male-skin-type-7-96.png"));
        imageIcon.setFitWidth(96);
        imageIcon.setFitHeight(96);
        imageIcon.setLayoutX(946);
        imageIcon.setLayoutY(42);   
        
        imageIcon.setOnMouseClicked(new EventHandler<MouseEvent>() {

            private FileChooser fileChooser = new FileChooser();
            @Override
            public void handle(MouseEvent event) {
                System.out.println("ImageView (imageIcon) clicked!");
                

                fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.jpeg", "*.gif"));

                File selectedFile = fileChooser.showOpenDialog(primaryStage);
                if (selectedFile != null) {
                    user.setImage(selectedFile);
                    Image selectedImage = new Image(selectedFile.toURI().toString());
                }
            }
        });
        
        userButton = new TextField();
        userButton.setFocusTraversable(false);
        userButton.setLayoutX(728);
        userButton.setLayoutY(208);
        userButton.setPrefWidth(500);
        userButton.setPrefHeight(50);
        userButton.setPromptText("UserName");
        userButton.setFont(Font.font("Arial Black", 17));

        locationButton = new TextField();
        locationButton.setFocusTraversable(false);
        locationButton.setLayoutX(729);
        locationButton.setLayoutY(446);
        locationButton.setPrefWidth(500);
        locationButton.setPrefHeight(50);
        locationButton.setPromptText("Location");
        locationButton.setFont(Font.font("Arial Black", 17));

        contactButton = new TextField();
        contactButton.setFocusTraversable(false);
        contactButton.setLayoutX(730);
        contactButton.setLayoutY(535);
        contactButton.setPrefWidth(500);
        contactButton.setPrefHeight(50);
        contactButton.setPromptText("Contact No.");
        contactButton.setFont(Font.font("Arial Black", 17));

        ToggleGroup genderToggleGroup = new ToggleGroup();

        maleButton = new RadioButton("Male");
        maleButton.setLayoutX(743);
        maleButton.setLayoutY(373);
        maleButton.setPrefWidth(165);
        maleButton.setPrefHeight(47);
        maleButton.setFont(Font.font("Arial Black", 25));
        maleButton.setToggleGroup(genderToggleGroup);


        otherButton = new RadioButton("Other");
        otherButton.setLayoutX(1038);
        otherButton.setLayoutY(379);
        otherButton.setFont(Font.font("Arial Black", 25));
        otherButton.setToggleGroup(genderToggleGroup);

        femaleButton = new RadioButton("Female");
        femaleButton.setLayoutX(872);
        femaleButton.setLayoutY(379);
        femaleButton.setFont(Font.font("Arial Black", 25));
        femaleButton.setToggleGroup(genderToggleGroup);

        Button signupButton = new Button("Signup");
        signupButton.setLayoutX(1091);
        signupButton.setLayoutY(606);
        signupButton.setPrefWidth(130);
        signupButton.setPrefHeight(42);
        signupButton.setStyle("-fx-background-color: #87ceeb;");
        signupButton.setFont(Font.font("Arial Black", 24));

       signupButton.setOnAction(e -> {
            
            user.setUsername(userButton.getText());
            user.setPassword(passwordButton.getText());
            user.setGender(maleButton.isSelected() ? "Male" : femaleButton.isSelected() ? "Female" : "Other");
            user.setLocation(locationButton.getText());
            user.setContact(contactButton.getText());

            if(user.getUsername().isEmpty() || user.getPassword().isEmpty() || user.getGender().isEmpty() || user.getLocation().isEmpty() || user.getContact().isEmpty()){
                Alert alert = new Alert(AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setContentText("Please fill in all the fields.");
                alert.showAndWait();
            }else if(user.getUsername().matches(".*\\d.*")) {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setContentText("Username should not contain numbers.");
                alert.showAndWait();
            }else{
                DBHandler obj = new DBHandler();
                try {
                    obj.handleSignup(user);
                    showPopupPane();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        Button backButton = new Button("Back");
        backButton.setLayoutX(47);
        backButton.setLayoutY(605);
        backButton.setPrefWidth(122);
        backButton.setPrefHeight(58);
        backButton.setFont(Font.font("Arial Black", 24));

        backButton.setOnAction(e->{
            loginscean login = new loginscean();
            login.start(primaryStage);
        });

        ImageView logo = new ImageView(new Image("file:book_revival/Photos/Logo.png"));
        logo.setFitWidth(72);
        logo.setFitHeight(84);
        logo.setLayoutX(11);
        logo.setLayoutY(5);

        passwordButton = new PasswordField();
        passwordButton.setLayoutX(727);
        passwordButton.setLayoutY(298);
        passwordButton.setPrefWidth(445);
        passwordButton.setPrefHeight(50);
        passwordButton.setPromptText("Password");
        passwordButton.setFont(Font.font("Arial Black", 17));

        ToggleButton toggleButton = new ToggleButton();
        toggleButton.setPrefSize(53, 50);
        toggleButton.setLayoutX(1169);
        toggleButton.setLayoutY(298);

        ImageView eyeIcon = new ImageView(new Image("file:book_revival/Photos/eye_Open.png"));
        eyeIcon.setFitHeight(25);
        eyeIcon.setFitWidth(25);

        ImageView eyeSlashIcon = new ImageView(new Image("file:book_revival/Photos/eye_hidden.png"));
        eyeSlashIcon.setFitHeight(25);
        eyeSlashIcon.setFitWidth(25);

        toggleButton.setGraphic(eyeIcon);

        toggleButton.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                passwordButton.setVisible(false);
                passwordButton.setManaged(false);

                TextField textField = new TextField(passwordButton.getText());
                textField.setPrefSize(445, 50);
                textField.setLayoutX(727);
                textField.setLayoutY(298);
                textField.setPromptText("Password");
                textField.setFont(Font.font("Arial Black", 17));
                textField.setAlignment(Pos.CENTER_LEFT);

                root.getChildren().remove(passwordButton);
                root.getChildren().add(1, textField);

                toggleButton.setGraphic(eyeSlashIcon);
            } else {
                TextField textField = (TextField) root.getChildren().get(1);
                passwordButton.setText(textField.getText());

                root.getChildren().remove(textField);
                root.getChildren().add(1, passwordButton);

                passwordButton.setVisible(true);
                passwordButton.setManaged(true);

                toggleButton.setGraphic(eyeIcon);
            }
        });

        root.getChildren().addAll(background, title, lab, userIcon, passwordIcon, genderIcon, locationIcon, callIcon,imageIcon,
                userButton, locationButton, contactButton, maleButton, otherButton, femaleButton, signupButton, backButton,
                logo, passwordButton,toggleButton);

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    private void showPopupPane() {
        Stage popupStage = new Stage();
        popupStage.initModality(Modality.APPLICATION_MODAL);
        popupStage.initStyle(StageStyle.UTILITY);
        popupStage.setTitle(" ");

        Pane popupPane = new Pane();
        popupPane.setPrefWidth(400);
        popupPane.setPrefHeight(200);
        ImageView imageView = new ImageView(new Image("file:book_revival/Photos/check-green.gif"));
        imageView.setFitWidth(100); 
        imageView.setFitHeight(100); 
        imageView.setLayoutX(50); 
        imageView.setLayoutY(50); 
        popupPane.getChildren().add(imageView); 


        Label label = new Label("Successfully Submitted !");
        label.setFont(Font.font("Arial Black", 15)); 
        label.setLayoutX(180); 
        label.setLayoutY(80); 
        popupPane.getChildren().add(label); 


        Scene popupScene = new Scene(popupPane);
        popupStage.setScene(popupScene);
        popupStage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }
}
