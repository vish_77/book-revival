package book_revival;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.image.Image;

public class DBHandler{

    Connection dbConnection;
    
         public Connection getConnection() {
            String connectionString = "jdbc:mysql://" + "localhost" + ":" + 3306 + "/" + "scenemakers";

            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
             } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

             try {
            // Create a database connection
                 dbConnection = DriverManager.getConnection(connectionString, "root", "Vishal@77");
            } catch (SQLException e) {
              e.printStackTrace();
            }
            return dbConnection;
        }

    private Connection connection;
    private PreparedStatement pst;

public void handleSignup(User user) throws IOException {
    Connection connection = getConnection();

    if (connection != null) {
      
        String insert = "INSERT INTO userinfo(UserName, Password, Gender, Address, Contact, Image ) VALUES (?, ?, ?, ?, ?, ?)";

        try (PreparedStatement pst = connection.prepareStatement(insert)) {
            pst.setString(1, user.getUsername());
            pst.setString(2, user.getPassword());
            pst.setString(3, user.getGender());
            pst.setString(4, user.getLocation());
            pst.setString(5, user.getContact());
            pst.setBytes(6, selectImage(user.getImage()));
            pst.executeUpdate();

            System.out.println("Signup successful");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    } else {
        System.err.println("Failed to establish a database connection.");
    }
}
public byte[] selectImage(File selectedFile) throws IOException{
    
        // Read the image into a byte array
        FileInputStream fis = new FileInputStream(selectedFile);
        byte[] imageBytes = new byte[(int) selectedFile.length()];
        fis.read(imageBytes);
        fis.close();
    return imageBytes;
}

public int sinup(User user){
    connection = getConnection();
            
            String q1 = "Select * from userinfo where UserName=? and Password=?";
            int x = 0;
            try{
                pst = connection.prepareStatement(q1);
                pst.setString(1,user.getUsername());  
                pst.setString(2,user.getPassword());
                ResultSet rs = pst.executeQuery();
                if (!rs.next()) {
                    System.out.println("No matching username and password found in the database");
                    x = -1;
                } else {
                    System.out.println("Hello welcome!");
                }

            }catch(SQLException ie){
                System.out.println();
            }
            return x;
    }
    public String getUserInfoFromDatabase(String username) {
            
        return "Sample User Information for " + username;
    }

    public List<Image> fetchImagesFromDatabaseForBranch(String BranchName, String Sem) throws SQLException {
        Connection connection = getConnection();
        List<Image> images = new ArrayList<>();
        System.out.println(BranchName);
        System.out.println(Sem);
        String query = "SELECT BookImage FROM book_info WHERE Semister = ? AND Branch = ?";
    
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, Sem);  // Set the value for the first parameter
            statement.setString(2, BranchName);  // Set the value for the second parameter
    
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    byte[] imageData = resultSet.getBytes("BookImage");
                    
                    System.out.println("countofsort");
                    Image image = new Image(new java.io.ByteArrayInputStream(imageData));
                    images.add(image);
                }
            }
        }
        return images;
    }
    
    public List<Image> fetchImagesFromDatabaseForBookName(String bookname) throws SQLException {
        Connection connection = getConnection();
        List<Image> images = new ArrayList<>();
        String query = "SELECT BookImage FROM book_info WHERE BookName = ?";
    
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, bookname);  // Set the value for the first parameter
    
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    byte[] imageData = resultSet.getBytes("BookImage");
    
                    Image image = new Image(new java.io.ByteArrayInputStream(imageData));
                    images.add(image);
                    
                }
            }
        }
        System.out.println("HelloImages");
        return images;
    }
    
    public Image fetchMyProfileImageFromDatabase(User user) {
        try (Connection connection = getConnection();
            PreparedStatement pst = connection.prepareStatement("SELECT Image FROM userinfo WHERE UserName= ? and Password= ?")) {
            pst.setString(1, user.getUsername());
            pst.setString(2, user.getPassword());
            try (ResultSet rs = pst.executeQuery()) {
                if (rs.next()) {
                    byte[] imageData = rs.getBytes("Image");
                    Image image = new Image(new ByteArrayInputStream(imageData));
                    System.out.println("Image fetched from the database");
                    return image;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void submitButtonAction(Book book,User user) throws IOException{
        Connection connection = getConnection();
        System.out.println("Successfulyy Entered");
        
        if (connection != null) {

            String q1 = "INSERT INTO book_info(Username, BookName, Edition, Publication, Semister, Price, BookImage, Branch) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            System.out.println(user.getUsername());
            System.out.println(book.getBookName());
                
            try (PreparedStatement pst = connection.prepareStatement(q1)) {
                pst.setString(1, user.getUsername());
                pst.setString(2, book.getBookName());
                pst.setString(3, book.getEdition());
                pst.setString(4, book.getPublication());
                pst.setString(5, book.getSemister());
                pst.setString(6, book.getPrice());
                pst.setBytes(7, selectImage(book.getBook_Image()));
                pst.setString(8, book.getBranch());
                pst.executeUpdate();

                System.out.println("Book_Info stored successfully");
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } else {
            System.err.println("Failed to establish a database connection.");
        }
    }

    public void UserInfo(String username, User user) {
        Connection connection = getConnection();

        if (connection != null) {
            String query = "SELECT Password, Gender, Address, Contact FROM userinfo WHERE UserName = ?";

            try (PreparedStatement pst = connection.prepareStatement(query)) {
                pst.setString(1, username);
                ResultSet resultSet = pst.executeQuery();

                if (resultSet.next()) {
                    String password = resultSet.getString("Password");
                    String gender = resultSet.getString("Gender");
                    String location = resultSet.getString("Address");
                    String contact = resultSet.getString("Contact");

                    user.setPassword(password);
                    user.setGender(gender);
                    user.setContact(contact);
                    user.setLocation(location);

                } else {
                    System.err.println("User not found in the database.");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } else {
            System.err.println("Failed to establish a database connection.");
        }
    }

    public void updateUserInfo(String username, User user) throws IOException {
        connection = getConnection();
        
        if (connection != null) {
            String updateQuery = "UPDATE userinfo SET UserName = ?, Password = ?, Gender = ?, Address = ?, Contact = ?, Image = ? WHERE UserName = ?";
    
            try (PreparedStatement pst = connection.prepareStatement(updateQuery)) {
                pst.setString(1, user.getUsername()); 
                pst.setString(2, user.getPassword());
                pst.setString(3, user.getGender());
                pst.setString(4, user.getLocation());
                pst.setString(5, user.getContact());
    
                // Handle null image
                byte[] imageBytes = null;
                if (user.getImage() != null) {
                    imageBytes = selectImage(user.getImage());
                }
    
                pst.setBytes(6, imageBytes);
                pst.setString(7, username);
    
                int rowsUpdated = pst.executeUpdate();
    
                if (rowsUpdated > 0) {
                    System.out.println("User information updated successfully");
                } else {
                    System.err.println("User not found in the database or no changes made.");
                }
            } catch (SQLException e) {
                e.printStackTrace();
                System.err.println("SQL Error: " + e.getMessage());
            } finally {
                try {
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } else {
            System.err.println("Failed to establish a database connection.");
        }
    }
    
    
    public List<Integer> getIdbookInfosForBookName(String bookname) {
        List<Integer> idbookInfos = new ArrayList<>();
        Connection connection = getConnection();
        if (connection != null) {
            String query = "SELECT idbook_info FROM book_info WHERE BookName = ?";
    
            try (PreparedStatement pst = connection.prepareStatement(query)) {
                pst.setString(1, bookname); // Set the parameter for bookname
                try (ResultSet resultSet = pst.executeQuery()) {
                    while (resultSet.next()) {
                        int id = resultSet.getInt("idbook_info");
                        idbookInfos.add(id);
                        System.out.println("id"+id);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } else {
            System.err.println("Failed to establish a database connection.");
        }
        System.out.println("id");
        return idbookInfos;
    }
    
    public List<Integer> getIdbookInfosForBranch(String branch, String sem) {
        List<Integer> idbookInfos = new ArrayList<>();
        Connection connection = getConnection();
    
        if (connection != null) {
            String query = "SELECT idbook_info FROM book_info WHERE Semister = ? AND Branch = ?";
    
            try (PreparedStatement pst = connection.prepareStatement(query)) {
                pst.setString(1, sem); // Set the value for the first placeholder
                pst.setString(2, branch); // Set the value for the second placeholder
    
                try (ResultSet resultSet = pst.executeQuery()) {
                    while (resultSet.next()) {
                        int id = resultSet.getInt("idbook_info");
                        idbookInfos.add(id);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } else {
            System.err.println("Failed to establish a database connection.");
        }
    
        return idbookInfos;
    }
    

    public Book buyBookInfo(Book book, int id) throws IOException {
        Connection connection = getConnection();
    
        if (connection != null) {
            String query = "SELECT Username, BookName, Edition, Publication, Semister, Price, BookImage, Branch FROM book_info WHERE idbook_info = ?";
    
            try (PreparedStatement pst = connection.prepareStatement(query)) {
                pst.setInt(1, id);
                ResultSet resultSet = pst.executeQuery();
    
                if (resultSet.next()) {
                    String username = resultSet.getString("Username");
                    String bookName = resultSet.getString("BookName");
                    String edition = resultSet.getString("Edition");
                    String publication = resultSet.getString("Publication");
                    String semister = resultSet.getString("Semister");
                    String price = resultSet.getString("Price");
    

                    byte[] bookImageBytes = resultSet.getBytes("BookImage");
                    File tempFile = File.createTempFile("book_image", ".png");
                    tempFile.deleteOnExit();
                    try (FileOutputStream fos = new FileOutputStream(tempFile)) {
                        fos.write(bookImageBytes);
                    }

                    String branch = resultSet.getString("Branch");
    
                    book.setUsername(username);
                    book.setBookName(bookName);
                    book.setEdition(edition);
                    book.setPublication(publication);
                    book.setSemister(semister);
                    book.setPrice(price);
                    book.setBook_Image(tempFile);
                    book.setBranch(branch);
                    

                } else {
                    System.err.println("Book not found in the database.");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } else {
            System.err.println("Failed to establish a database connection.");
        }

        return book;
    }

    public void loactionUser(ClientInfo client, String username) {
        Connection connection = getConnection();
        System.out.println(username);
        if (connection != null) {
            String query = "SELECT Address, Contact FROM userinfo WHERE UserName = ?";
    
            try (PreparedStatement pst = connection.prepareStatement(query)) {
                pst.setString(1, username);
                ResultSet resultSet = pst.executeQuery();
    
                if (resultSet.next()) {
                    String address = resultSet.getString("Address");
                    String contact = resultSet.getString("Contact");
                    
                    client.setAddress(address);
                    client.setContact(contact);
                } else {
                    System.err.println("User not found in the database.");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } else {
            System.err.println("Failed to establish a database connection.");
        }
    }
    
    
    // In your DBHandler class

    public boolean doesDataExist(String searchTerm) {
        connection = getConnection(); // Use the class-level connection
        String sql = "SELECT COUNT(*) FROM book_info WHERE BookName LIKE ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, "%" + searchTerm + "%");
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    int count = resultSet.getInt(1);
                    System.out.println(count);
                    return count > 1; 
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false; // Database error or no matches found
    }
}


