package book_revival;

import java.io.File;

public class Book {
    private String Username;
    private String BookName;
    private String Edition;
    private String Publication;
    private String Semister;
    private String Price;
    private String Location;
    public String getUsername() {
        return Username;
    }
    public void setUsername(String username) {
        Username = username;
    }
    private File Book_Image;
    private String Branch;

    public String getBranch() {
        return Branch;
    }
    public void setBranch(String branch) {
        Branch = branch;
    }
    public String getBookName() {
        return BookName;
    }
    public void setBookName(String bookName) {
        BookName = bookName;
    }
    public String getEdition() {
        return Edition;
    }
    public void setEdition(String edition) {
        Edition = edition;
    }
    public String getPublication() {
        return Publication;
    }
    public void setPublication(String publication) {
        Publication = publication;
    }
    public String getSemister() {
        return Semister;
    }
    public void setSemister(String semister) {
        Semister = semister;
    }
    public String getPrice() {
        return Price;
    }
    public void setPrice(String price) {
        Price = price;
    }
    public File getBook_Image() {
        return Book_Image;
    }
    public void setBook_Image(File book_Image) {
        Book_Image = book_Image;
    }
    public String getLocation() {
        return Location;
    }
    public void setLocation(String location) {
        Location = location;
    }
}
