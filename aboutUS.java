
package book_revival;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.paint.Color;

public class aboutUS extends Application {

    private User user;
    
    public void setUser(User user) {
        this.user = user;
    }
    @Override
    public void start(Stage primaryStage){
        
        AnchorPane anchorPane = new AnchorPane();
        anchorPane.setPrefWidth(1400);
        anchorPane.setPrefHeight(700);
        anchorPane.setStyle("-fx-background-color: linear-gradient(to bottom,#4B79A1,#283E51);");


      
        ImageView imageView = new ImageView(new Image("file:book_revival/Photos/core2web.png"));
        imageView.setFitWidth(300);
        imageView.setFitHeight(91);
        AnchorPane.setTopAnchor(imageView, 567.0);
        AnchorPane.setLeftAnchor(imageView, 312.0);

      
        Text text = new Text("\"In collaboration with Core2Web, an education and tech supporter, we've built a platform for pre-loved books to find new homes. Our mission is to spread knowledge and create a brighter future.\"");
        text.setFill(Color.web("#000000"));
        text.setLayoutX(120);
        text.setLayoutY(332);
        text.setFont(Font.font("System", 28));
        text.setTextAlignment(javafx.scene.text.TextAlignment.CENTER);
        text.setWrappingWidth(623.65234375);

        
        Text membersText = new Text("Members :");
        membersText.setFill(Color.web("#000000"));
        membersText.setLayoutX(1018);
        membersText.setLayoutY(246);
        membersText.setFont(Font.font("System", 47));

      
        Text teamMembersText = new Text("Kunal Pardeshi Tejas Jawale Nilesh Sartape Vishal More");
        teamMembersText.setFill(Color.web("000000"));
        teamMembersText.setLayoutX(1016);
        teamMembersText.setLayoutY(346);
        teamMembersText.setFont(Font.font("System Bold Italic", 43));
        teamMembersText.setWrappingWidth(311.826171875);

     
        ImageView image1 = new ImageView(new Image("file:book_revival/Photos/insta.png"));
        ImageView image2 = new ImageView(new Image("file:book_revival/Photos/linked.png"));
        ImageView image3 = new ImageView(new Image("file:book_revival/Photos/web.png"));

        ImageView image4 = new ImageView(new Image("file:book_revival/Photos/bookLogo.png"));
       

        image1.setFitWidth(69);
        image1.setFitHeight(67);
        AnchorPane.setTopAnchor(image1, 593.0);
        AnchorPane.setLeftAnchor(image1, 1024.0);

        image1.setOnMouseClicked(event -> {
            String url = "https://instagram.com/core2web?igshid=MzRlODBiNWFlZA=="; 
            try {
                java.awt.Desktop.getDesktop().browse(new java.net.URI(url));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        

        image2.setFitWidth(86);
        image2.setFitHeight(90);
        AnchorPane.setTopAnchor(image2, 580.0);
        AnchorPane.setLeftAnchor(image2, 1118.0);

         image2.setOnMouseClicked(event -> {
            String url = "https://www.linkedin.com/company/core2web-technologies/"; 
            try {
                java.awt.Desktop.getDesktop().browse(new java.net.URI(url));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        image3.setFitWidth(57);
        image3.setFitHeight(58);
        AnchorPane.setTopAnchor(image3, 595.0);
        AnchorPane.setLeftAnchor(image3, 1234.0);

        image3.setOnMouseClicked(event -> {
            String url = "https://www.core2web.in/"; 
            try {
                java.awt.Desktop.getDesktop().browse(new java.net.URI(url));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });


        image4.setFitWidth(235);
        image4.setFitHeight(190); 
        AnchorPane.setTopAnchor(image4, 72.0); 
        AnchorPane.setLeftAnchor(image4, 308.0);

        Button button = new Button("Back");
        button.setLayoutX(58);
        button.setLayoutY(613);
        button.setPrefWidth(145);
        button.setPrefHeight(47);
        button.setStyle("-fx-background-color: linear-gradient(to top, #005aa7, #fffde4,#005aa7);-fx-background-radius: 40;");
        button.setFont(Font.font(20));

        button.setOnAction(e->{

            MainPage main = new MainPage();
            main.setUser(user);
            main.start(primaryStage); 
        });

       
        anchorPane.getChildren().addAll(
            imageView, text, membersText, teamMembersText,
            image1, image2, image3,image4, button
        );

    
        Scene scene = new Scene(anchorPane);
        primaryStage.setScene(scene);

        primaryStage.setTitle("About Us Page");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}