package book_revival;

import java.io.File;
import java.net.MalformedURLException;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class buyBookpage extends Application {

    public User user;
    
    public void setUser(User user) {
        this.user = user;
    }

    public Book book;

    public void setBook(Book book){
        this.book = book;
    }
    DBHandler dbHandler = new DBHandler();
    ClientInfo client = new ClientInfo();
    @Override
    public void start(Stage primaryStage) throws MalformedURLException {

        dbHandler.loactionUser(client,book.getUsername());
        AnchorPane anchorPane = new AnchorPane();
        anchorPane.setPrefWidth(1400);
        anchorPane.setPrefHeight(700);
        anchorPane.setStyle("-fx-background-color: linear-gradient(to bottom,#373B44,#4286f4);");

        File bookImageFile = book.getBook_Image();

        ImageView imageView = new ImageView();
        imageView.setFitWidth(308);
        imageView.setFitHeight(440);
        imageView.setLayoutX(138);
        imageView.setLayoutY(97);

        if (bookImageFile != null && bookImageFile.exists()) {
            Image image = new Image(bookImageFile.toURI().toURL().toExternalForm());
            imageView.setImage(image);
        }

        Label labelBookName = new Label("Book Name:");
        labelBookName.setLayoutX(628);
        labelBookName.setLayoutY(131);
        labelBookName.setPrefWidth(212);
        labelBookName.setPrefHeight(95);
        labelBookName.setFont(Font.font("Britannic Bold", 33));
        
        Label labelBranch = new Label("Branch:");
        labelBranch.setLayoutX(628);
        labelBranch.setLayoutY(207);
        labelBranch.setPrefWidth(202);
        labelBranch.setPrefHeight(95);
        labelBranch.setFont(Font.font("Britannic Bold", 33));

        Label labelPublication = new Label("Publication:");
        labelPublication.setLayoutX(628);
        labelPublication.setLayoutY(357);
        labelPublication.setPrefWidth(202);
        labelPublication.setPrefHeight(95);
        labelPublication.setFont(Font.font("Britannic Bold", 33));

        Label labelEdition = new Label("Edition:");
        labelEdition.setLayoutX(628);
        labelEdition.setLayoutY(283);
        labelEdition.setPrefWidth(134);
        labelEdition.setPrefHeight(95);
        labelEdition.setFont(Font.font("Britannic Bold", 33));

        Label labelSellerName = new Label("Price:");
        labelSellerName.setLayoutX(628);
        labelSellerName.setLayoutY(425);
        labelSellerName.setPrefWidth(218);
        labelSellerName.setPrefHeight(95);
        labelSellerName.setFont(Font.font("Britannic Bold", 33));

        Label textFieldBookName = new Label(book.getBookName());
        textFieldBookName.setLayoutX(860);
        textFieldBookName.setLayoutY(161);
        textFieldBookName.setPrefWidth(405);
        textFieldBookName.setPrefHeight(41);
        textFieldBookName.setFont(Font.font("System Bold Italic", 33));
        
        Label textFieldBranch = new Label(book.getBranch());
        textFieldBranch.setLayoutX(860);
        textFieldBranch.setLayoutY(237);
        textFieldBranch.setPrefWidth(405);
        textFieldBranch.setPrefHeight(41);
        textFieldBranch.setFont(Font.font("System Bold Italic", 33));

        Label textFieldPublication = new Label(book.getPublication());
        textFieldPublication.setLayoutX(860);
        textFieldPublication.setLayoutY(387);
        textFieldPublication.setPrefWidth(405);
        textFieldPublication.setPrefHeight(41);
        textFieldPublication.setFont(Font.font("System Bold Italic", 33));

        Label textFieldEdition = new Label(book.getEdition());
        textFieldEdition.setLayoutX(860);
        textFieldEdition.setLayoutY(314);
        textFieldEdition.setPrefWidth(405);
        textFieldEdition.setPrefHeight(41);
        textFieldEdition.setFont(Font.font("System Bold Italic", 33));

        Label textFieldSellerName = new Label(book.getPrice());
        textFieldSellerName.setLayoutX(860);
        textFieldSellerName.setLayoutY(454);
        textFieldSellerName.setPrefWidth(405);
        textFieldSellerName.setPrefHeight(41);
        textFieldSellerName.setFont(Font.font("System Bold Italic", 33));

        Button buttonBuyNow = new Button("Buy Now");
        buttonBuyNow.setLayoutX(812);
        buttonBuyNow.setLayoutY(542);
        buttonBuyNow.setPrefWidth(254);
        buttonBuyNow.setPrefHeight(87);
        buttonBuyNow.setStyle("-fx-background-color: linear-gradient(to top, #ffffff,#fffc00,#ffffff);-fx-background-radius: 40;");
        buttonBuyNow.setFont(Font.font(41));

        Button buttonBack = new Button("Back");
        buttonBack.setLayoutX(84);
        buttonBack.setLayoutY(605);
        buttonBack.setPrefWidth(138);
        buttonBack.setPrefHeight(29);
        buttonBack.setStyle("-fx-background-color: linear-gradient(to top, #005aa7, #fffde4,#005aa7);-fx-background-radius: 40;");
        buttonBack.setFont(Font.font(20));

        buttonBack.setOnAction(e->{

            MainPage main = new MainPage();
            main.setUser(user);
            main.start(primaryStage);
        });

        buttonBuyNow.setOnAction(e -> showPopupPane());

        anchorPane.getChildren().addAll(
            imageView, labelBookName, labelBranch, labelPublication, labelEdition, labelSellerName,
            textFieldBookName, textFieldBranch,textFieldPublication, textFieldEdition, textFieldSellerName,
            buttonBuyNow, buttonBack 
        );

        Scene scene = new Scene(anchorPane);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Book Details Page");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void showPopupPane() {
        Stage popupStage = new Stage();
        popupStage.initModality(Modality.APPLICATION_MODAL);
        popupStage.initStyle(StageStyle.UTILITY);
        popupStage.setTitle(" ");

        Pane popupPane = new Pane();
        popupPane.setPrefWidth(500);
        popupPane.setPrefHeight(250);

    Label label1 = new Label("Seller Name :");
    label1.setLayoutX(70);  
    label1.setLayoutY(45);  
    Font labelFont1 = Font.font("System Bold Italic", 25);  
    label1.setFont(labelFont1);

    Label label2 = new Label(book.getUsername());
    label2.setLayoutX(252);  
    label2.setLayoutY(45);   
    Font labelFont2 = Font.font("System Bold Italic", 25);  
    label2.setFont(labelFont2);

    Label label3 = new Label("Contact :");
    label3.setLayoutX(70);  
    label3.setLayoutY(110);  
    Font labelFont3 = Font.font("System Bold Italic", 25);  
    label3.setFont(labelFont3);

    Label label4 = new Label(client.getContact());
    label4.setLayoutX(252);  
    label4.setLayoutY(110); 
    Font labelFont4 = Font.font("System Bold Italic", 25);  
    label4.setFont(labelFont4); 

    Label label5 = new Label("Address:");
    label5.setLayoutX(70);  
    label5.setLayoutY(175); 
    Font labelFont5 = Font.font("System Bold Italic", 25);  
    label5.setFont(labelFont4);

    Label label6 = new Label(client.getAddress());
    label6.setLayoutX(252);  
    label6.setLayoutY(175); 
    Font labelFont6 = Font.font("System Bold Italic", 25);  
    label6.setFont(labelFont4);

    BackgroundImage backgroundImage = new BackgroundImage(
        new Image("file:F:/super-x/book_revival/Photos/blur3.jpg", 450, 234, false, true),
        BackgroundRepeat.NO_REPEAT,
        BackgroundRepeat.NO_REPEAT,
        BackgroundPosition.DEFAULT,
        BackgroundSize.DEFAULT
    );

    popupPane.getChildren().addAll(label1, label2, label3, label4, label5, label6);
    popupPane.setBackground(new Background(backgroundImage));

        Scene popupScene = new Scene(popupPane);
        popupStage.setScene(popupScene);

        popupStage.show();
    }
}