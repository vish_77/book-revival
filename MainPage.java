package book_revival;

import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import javafx.animation.ScaleTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.geometry.Orientation;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
//import javafx.util.Duration;

public class MainPage extends Application {

    DBHandler DbHandler = new DBHandler();
    private User user;

    public void setUser(User user) {
        this.user = user;
    }

    List<Image> images = new ArrayList<>();
    List<Integer> idbookInfos = new ArrayList<>();
    private String SelecteFile;

    @Override
    public void start(Stage primaryStage) {
        // Create an AnchorPane, the root element
        AnchorPane root = new AnchorPane();
        root.setPrefWidth(1400.0);
        root.setPrefHeight(700.0);

        Image backgroundImage = new Image("file:book_revival/Photos/bg1.jpg"); 

      
        String backgroundImageStyle = "-fx-background-image: url('" + backgroundImage.impl_getUrl() + "'); " +
                "-fx-background-size:cover ;"; 
        
        root.setStyle(backgroundImageStyle);

         ImageView dashImg1 = new ImageView(new Image("file:book_revival/Photos/4.png"));
        dashImg1.setFitWidth(492.0);
        dashImg1.setFitHeight(460.0);
        dashImg1.setLayoutX(915.0);
        dashImg1.setLayoutY(320.0);
        dashImg1.setPreserveRatio(true);
        
         

        ImageView dashImg2 = new ImageView(new Image("file:book_revival/Photos/6.png"));
        dashImg2.setFitWidth(483.0);
        dashImg2.setFitHeight(489.0);
        dashImg2.setLayoutX(-6.0);
        dashImg2.setLayoutY(250.0);
        dashImg2.setPreserveRatio(true);

        Text headlines = new Text("Discover the World of Pre-Loved Books");
        headlines.setFont(Font.font("Felix Titling", 44.0));
        headlines.setLayoutX(295.0);
        headlines.setLayoutY(290.0);
        headlines.setTextAlignment(javafx.scene.text.TextAlignment.CENTER);
        headlines.setWrappingWidth(646.361328125);

        Text description = new Text("Welcome to Book Revival, your gateway to an extensive collection of secondhand books at unbeatable prices. Immerse yourself in a world of well-loved stories, hidden treasures, and literary adventures waiting to be explored.");
        description.setFont(Font.font("Lucida Bright Italic", 32.0));
        description.setLayoutX(370.0);
        description.setLayoutY(405.0);
        description.setWrappingWidth(500.0);
        description.setTextAlignment(TextAlignment.CENTER);

        ImageView c2w_logo= new ImageView(new Image("file:book_revival/Photos/Logo.png"));
        c2w_logo.setFitWidth(155.0);
        c2w_logo.setFitHeight(145.0);
        c2w_logo.setLayoutX(1200.0);
        c2w_logo.setLayoutY(5.0);

        javafx.util.Duration duration = javafx.util.Duration.millis(1000);

        // Create a ScaleTransition with the specified duration
        ScaleTransition scaleTransition = new ScaleTransition(duration, c2w_logo);
        scaleTransition.setToX(-1.0);
        scaleTransition.setToY(1.0);
        
        // Create a SequentialTransition and set the cycle count
        SequentialTransition sequentialTransition = new SequentialTransition(scaleTransition);
        sequentialTransition.setCycleCount(Timeline.INDEFINITE);
        
        // Play the animation
        sequentialTransition.play();

        Pane contactUs_pane = new Pane();
        contactUs_pane.setLayoutX(445);
        contactUs_pane.setLayoutY(225);
        contactUs_pane.setPrefSize(900, 525);
        contactUs_pane.setStyle("-fx-background-color:#6DD5FA;" + 
        "-fx-background-radius: 30 30 30 30;");
        contactUs_pane.setVisible(false);


        Text text1 = new Text("sceneMaker2023@gmail.com ");
        text1.setLayoutX(61.0);
        text1.setLayoutY(209.0);
        text1.setFont(Font.font("Felix Titling", 25.0));

        Text text2 = new Text("contact :");
        text2.setLayoutX(60.0);
        text2.setLayoutY(87.0);
        text2.setFont(Font.font("Felix Titling", 25.0));

        Text text3 = new Text("Mob :");
        text3.setLayoutX(63.0);
        text3.setLayoutY(266.0);
        text3.setFont(Font.font("Felix Titling", 25.0));

        Text text4 = new Text("Email :");
        text4.setLayoutX(61.0);
        text4.setLayoutY(169.0);
        text4.setFont(Font.font("Felix Titling", 25.0));

        Text text5 = new Text("902257XX98 / 70100565XX");
        text5.setLayoutX(65.0);
        text5.setLayoutY(319.0);
        text5.setFont(Font.font("Felix Titling", 25.0));

        Text text6 = new Text("Address :");
        text6.setLayoutX(70.0);
        text6.setLayoutY(375.0);
        text6.setWrappingWidth(141.4658203125);
        text6.setFont(Font.font("Felix Titling", 25.0));

        Text text7 = new Text("Core2web, 3rd floor, Walhekar Properties,◄ Narhe, Pune-411041.");
        text7.setLayoutX(69.0);
        text7.setLayoutY(428.0);
        text7.setWrappingWidth(374.0);
        text7.setFont(Font.font("Felix Titling", 25.0));

        contactUs_pane.getChildren().addAll(text1,text2,text3,text4,text5,text6,text7);


        // Create a SplitPane
        SplitPane splitPane = new SplitPane();
        splitPane.setDividerPositions(0.7866666666666666);
        splitPane.setPrefWidth(1402.0);
        splitPane.setPrefHeight(152.0);
        splitPane.setOrientation(Orientation.VERTICAL);

        // Create the first AnchorPane inside the SplitPane
        AnchorPane anchorPane1 = new AnchorPane();
        anchorPane1.setPrefWidth(1400.0);
        anchorPane1.setPrefHeight(52.0);

        Image backgroundImage1 = new Image("file:book_revival/Photos/bg1.jpg"); 

      
        String backgroundImageStyle2 = "-fx-background-image: url('" + backgroundImage1.impl_getUrl() + "'); " +
                "-fx-background-size:cover ;"; 
        
        anchorPane1.setStyle(backgroundImageStyle2);


         Pane content1 = new Pane();
         content1.setPrefSize(130, 152);

         Pane content2 = new Pane();
         content2.setPrefSize(130, 152);

         Pane content3 = new Pane();
         content3.setPrefSize(130, 152);



        // Create the ImageView inside anchorPane1
       
        ImageView logoImageView = new ImageView(new Image("file:book_revival/Photos/bookLogo.png"));
        logoImageView.setFitWidth(136.0);
        logoImageView.setFitHeight(133.0);
        logoImageView.setLayoutX(63.0);
        logoImageView.setLayoutY(6.0);

        // Create the TextField inside anchorPane1
        TextField searchField = new TextField();
        searchField.setPrefWidth(575.0);
        searchField.setPrefHeight(26.0);
        searchField.setLayoutX(278.0);
        searchField.setLayoutY(39.0);
        searchField.setPromptText("         Search in...");
        searchField.setStyle("-fx-background-radius: 40;");
        searchField.setFont(new Font(18.0));
        DropShadow dropShadow = new DropShadow();
        dropShadow.setRadius(5);
        dropShadow.setOffsetX(3);
        dropShadow.setOffsetY(3);
        dropShadow.setColor(javafx.scene.paint.Color.GRAY);

        // Apply the shadow effect to the TextField
        searchField.setEffect(dropShadow);

       // Create the ImageView for search inside anchorPane1
        ImageView searchImageView = new ImageView(new Image("file:book_revival/Photos/search.png"));
        searchImageView.setFitWidth(31.0);
        searchImageView.setFitHeight(26.0);
        searchImageView.setLayoutX(805.0);
        searchImageView.setLayoutY(45.0);
        searchImageView.setOpacity(0.95);


        // Create the second AnchorPane inside the SplitPane
        AnchorPane anchorPane2 = new AnchorPane();
        anchorPane2.setPrefWidth(1400.0);
        anchorPane2.setPrefHeight(34.0);
        anchorPane2.setStyle("-fx-background-color: #0674BE;");

        ScrollPane scrollPane1 = new ScrollPane(content1);
        scrollPane1.setLayoutX(622);
        scrollPane1.setLayoutY(185);
        scrollPane1.setPrefSize(130, 152);
        scrollPane1.setVisible(false);

        ScrollPane scrollPane2 = new ScrollPane(content2);
        scrollPane2.setLayoutX(820);
        scrollPane2.setLayoutY(185);
        scrollPane2.setPrefSize(130, 152);
        scrollPane2.setVisible(false);
        
        ScrollPane scrollPane3 = new ScrollPane(content3);
        scrollPane3.setLayoutX(1020);
        scrollPane3.setLayoutY(185);
        scrollPane3.setPrefSize(130, 152);
        scrollPane3.setVisible(false);

        ScrollPane largeScrollPane = new ScrollPane();
        largeScrollPane.setLayoutX(445);
        largeScrollPane.setLayoutY(190);
        largeScrollPane.setPrefSize(960, 580);
        largeScrollPane.setVisible(false); 
        largeScrollPane.setStyle("-fx-background-color:#0674BE");
        
        // Inside your start method in MainPage class


        //String SearchBar = searchField.getText();

        Pane menuPane = new Pane();
        menuPane.setLayoutX(-1);
        menuPane.setLayoutY(220);
        menuPane.setPrefSize(389, 525);
        menuPane.setStyle("-fx-background-color:#6DD5FA;" + 
        "-fx-background-radius: 00 30 30 00;");
        menuPane.setVisible(false);

         DropShadow dropShadow2 = new DropShadow();
        dropShadow2.setRadius(10.0);
        dropShadow2.setOffsetX(5.0);
        dropShadow2.setOffsetY(5.0);
        dropShadow2.setColor(Color.GRAY);
        menuPane.setEffect(dropShadow);

        Button a2 = new Button();
        a2.setLayoutX(-25.0);
        a2.setLayoutY(20.0);
        a2.setPrefWidth(442.0);
        a2.setPrefHeight(104.0);
      

        Button a1 = new Button();
        a1.setLayoutX(-25.0);
        a1.setLayoutY(20.0);
        a1.setPrefWidth(389.0);
        a1.setPrefHeight(104.0);
        a1.setStyle("-fx-background-radius: 10;" + 
        "-fx-background-color: linear-gradient(to right, #eeb86d,#9946b2);");
          a2.setStyle("-fx-background-radius: 10;" + 
        "-fx-background-color: linear-gradient(to right, #eeb86d,#9946b2);");
        a2.setVisible(false);

       
        Button b2 = new Button();
        b2.setLayoutX(-25.0);
        b2.setLayoutY(145.0);
        b2.setPrefWidth(442.0);
        b2.setPrefHeight(104.0);
        b2.setStyle("-fx-background-radius: 10;" + 
        "-fx-background-color: linear-gradient(to right, #eeb86d,#9946b2);");
        b2.setVisible(false);
        

        Button b1 = new Button();
        b1.setLayoutX(-25.0);
        b1.setLayoutY(145.0);
        b1.setPrefWidth(389.0);
        b1.setPrefHeight(104.0);
        b1.setStyle("-fx-background-radius: 10;" + 
        "-fx-background-color: linear-gradient(to right, #eeb86d,#9946b2);");
        
        Button c2 = new Button();
        c2.setLayoutX(-25.0);
        c2.setLayoutY(270.0);
        c2.setPrefWidth(442.0);
        c2.setPrefHeight(104.0);
        c2.setStyle("-fx-background-radius: 10;" + 
        "-fx-background-color: linear-gradient(to right, #eeb86d,#9946b2);");
        c2.setVisible(false);
        

        Button c1 = new Button();
        c1.setLayoutX(-25.0);
        c1.setLayoutY(270.0);
        c1.setPrefWidth(389.0);
        c1.setPrefHeight(104.0);
        c1.setStyle("-fx-background-radius: 10;" + 
        "-fx-background-color: linear-gradient(to right, #eeb86d,#9946b2);");
        
        Button d2 = new Button();
        d2.setLayoutX(-25.0);
        d2.setLayoutY(395.0);
        d2.setPrefWidth(442.0);
        d2.setPrefHeight(104.0);
        d2.setStyle("-fx-background-radius: 10;" + 
        "-fx-background-color: linear-gradient(to right, #eeb86d,#9946b2);");
        d2.setVisible(false);
        

        Button d1 = new Button();
        d1.setLayoutX(-25.0);
        d1.setLayoutY(395.0);
        d1.setPrefWidth(389.0);
        d1.setPrefHeight(104.0);
        d1.setStyle("-fx-background-radius: 10;" + 
        "-fx-background-color: linear-gradient(to right, #eeb86d,#9946b2);");
        


        a2.setOnAction(event -> {
              a2.setVisible(false);
              a1.setVisible(true);
        });
       a1.setOnAction(event -> {
             a1.setVisible(false);
             a2.setVisible(true);
        });


        b2.setOnAction(event -> {
              b2.setVisible(false);
              b1.setVisible(true);
        });
        b1.setOnAction(event -> {
             b1.setVisible(false);
             b2.setVisible(true);
        });

        c2.setOnAction(event -> {
             
              c2.setVisible(false);
              c1.setVisible(true);

        });
        c1.setOnAction(event -> {
             c1.setVisible(false);
             c2.setVisible(true);
        });

        d2.setOnAction(event -> {
            d2.setVisible(false);
            d1.setVisible(true);
        });
        d1.setOnAction(event -> {
           d1.setVisible(false);
           d2.setVisible(true);
        });

        Button menu_btn = new Button("              MENU");
        menu_btn.setPrefWidth(390.0); 
        menu_btn.setPrefHeight(40.0); 
        menu_btn.setLayoutX(23.0);
        menu_btn.setLayoutY(-2.00);
        menu_btn.setStyle(
         "-fx-prompt-text-fill: white;" + 
         "-fx-font-size: 20px;" + "-fx-background-color: #0674BE;"+
         "-fx-alignment: CENTER-LEFT;"
        );

        ImageView menu_png = new ImageView(new Image("file:book_revival/Photos/menu_Png.png"));
        menu_png.setLayoutX(66.0);
        menu_png.setLayoutY(-70.0);
        menu_png.setFitWidth(46);
        menu_png.setFitHeight(36);
        
        ImageView seller_png = new ImageView(new Image("file:book_revival/Photos/sellerPng.png"));
        seller_png.setLayoutX(19.0);
        seller_png.setLayoutY(28.0);
        seller_png.setFitWidth(100);
        seller_png.setFitHeight(92);

        seller_png.setOnMouseClicked(e->{
            BookSellPage sell = new BookSellPage();
            sell.setUser(user);
            sell.start(primaryStage);
        });
        
        ImageView profile_png = new ImageView(new Image("file:book_revival/Photos/profileEdit.png"));
        profile_png.setLayoutX(7.0);
        profile_png.setLayoutY(137.0);
        profile_png.setFitWidth(132);
        profile_png.setFitHeight(126);

        profile_png.setOnMouseClicked(e->{

            UserInfoPage userInfo = new UserInfoPage();
            userInfo.setUser(user);
            userInfo.start(primaryStage);
        });
        
        ImageView help_png = new ImageView(new Image("file:book_revival/Photos/help.png"));
        help_png.setLayoutX(17.0);
        help_png.setLayoutY(271.0);
        help_png.setFitWidth(98);
        help_png.setFitHeight(106);
        
        help_png.setOnMouseClicked(e->{
            contactUs_pane.setVisible(true);
        });
        
        ImageView aboutUs_png = new ImageView(new Image("file:book_revival/Photos/aboutUs.png"));
        aboutUs_png.setLayoutX(11.0);
        aboutUs_png.setLayoutY(378.0);
        aboutUs_png.setFitWidth(108);
        aboutUs_png.setFitHeight(130);

        aboutUs_png.setOnMouseClicked(e->{
            aboutUS aboutus = new aboutUS();
            aboutus.setUser(user);
            aboutus.start(primaryStage);
            
        });
        
        Text sellerText = new Text("SELLER");
        sellerText.setFont(new Font(33.0));
        sellerText.setLayoutX(154.0);
        sellerText.setLayoutY(82.0);
    

        Text editProfilText = new Text("PROFILE");
        editProfilText.setFont(new Font(33.0));
        editProfilText.setLayoutX(154.0);
        editProfilText.setLayoutY(214.0);
    

        Text contacUsText = new Text("CONTACT US");
        contacUsText.setFont(new Font(33.0));
        contacUsText.setLayoutX(154.0);
        contacUsText.setLayoutY(338.0);
      
        Text aboutUsText2 = new Text("ABOUT US");
        aboutUsText2.setFont(new Font(33.0));
        aboutUsText2.setLayoutX(154.0);
        aboutUsText2.setLayoutY(442.0);
      

        menuPane.getChildren().addAll(a2,a1,sellerText,seller_png,b2,b1,editProfilText,profile_png,c2,c1,contacUsText,help_png,d2,d1,aboutUsText2,
                                        aboutUs_png);
        
       
        Button home_btn = new Button("HOME");
        home_btn.setPrefWidth(100.0); 
        home_btn.setPrefHeight(40.0); 
        home_btn.setLayoutX(438.0);
        home_btn.setLayoutY(-2.0); 
        home_btn.setCursor(Cursor.HAND);
        home_btn.setStyle(
         "-fx-prompt-text-fill: white;" + 
         "-fx-font-size: 20px;" + "-fx-background-color: #0674BE;"
        );        


        ComboBox<String> comboBox3 = new ComboBox<>();
        comboBox3.setPrefWidth(130.0);
        comboBox3.setPrefHeight(40.0);
        comboBox3.setLayoutX(620.0);
        comboBox3.setLayoutY(-2.0);
        comboBox3.setPromptText("SPPU");
        comboBox3.setCursor(Cursor.HAND);
        comboBox3.getItems().addAll("I","II","III","IV","V","VI","VII","VIII");
        comboBox3.setStyle(
         "-fx-prompt-text-fill: white;" + 
         "-fx-font-size: 20px;" + "-fx-background-color: #0674BE;"
        );

        class StringWrapper {
            String value = null;
            
            public StringWrapper(String value) {
                this.value = value;
            }
        
            public void set(String value) {
                this.value = value;
            }
        
            public String get() {
                return value;
            }
        }
        
        // ...
        
        StringWrapper selectedSem = new StringWrapper(null);
        comboBox3.setOnAction(event -> {
            String selectedValue = comboBox3.getValue();
            System.out.println(selectedValue);
            //selectedSem.set(selectedValue);
        
            if (selectedValue != null) {
                selectedSem.set(selectedValue);
                scrollPane1.setVisible(true);
            }
            largeScrollPane.setVisible(false);
            dashImg1.setEffect(new GaussianBlur());
            dashImg2.setEffect(new GaussianBlur());
            headlines.setEffect(new GaussianBlur());
            description.setEffect(new GaussianBlur());
        });
    

        Button button1 = new Button("CS");
        button1.setPrefWidth(130);
        button1.setPrefHeight(40);
        button1.setLayoutX(0);
        button1.setLayoutY(-2);

        button1.setOnAction(event -> {
             String selectedSemValue = selectedSem.get();
            scrollPane1.setVisible(false);
            SelecteFile = "CS";
            System.out.println("slectedsemValue "+selectedSemValue);
            try {
                images = DbHandler.fetchImagesFromDatabaseForBranch(SelecteFile,selectedSemValue);
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            idbookInfos = DbHandler.getIdbookInfosForBranch(SelecteFile,selectedSemValue);
            ////////////RUNTIME FUNCTIONALITY VISIBILITY;      
            comboBox3.getSelectionModel().clearSelection();
            comboBox3.setPromptText("SPPU");    
            largeScrollPane.setVisible(true); 
            createImageViewsAndHBox(primaryStage,largeScrollPane);
        });

        Button button2 = new Button("IT");
        button2.setPrefWidth(130);
        button2.setPrefHeight(40);
        button2.setLayoutX(0);
        button2.setLayoutY(26);
         button2.setOnAction(event -> {
            String selectedSemValue = selectedSem.get();
            SelecteFile = "IT";
            scrollPane1.setVisible(false);
            try {
                images = DbHandler.fetchImagesFromDatabaseForBranch(SelecteFile,selectedSemValue);
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            idbookInfos = DbHandler.getIdbookInfosForBranch(SelecteFile,selectedSemValue);
            ////////////RUNTIME FUNCTIONALITY VISIBILITY;      
            comboBox3.getSelectionModel().clearSelection();
            comboBox3.setPromptText("SPPU");    
            largeScrollPane.setVisible(true); 
            createImageViewsAndHBox(primaryStage,largeScrollPane);
        });

         Button button3 = new Button("ENTC");
        button3.setPrefWidth(130);
        button3.setPrefHeight(40);
        button3.setLayoutX(0);
        button3.setLayoutY(54);
         button3.setOnAction(event -> {
            String selectedSemValue = selectedSem.get();
            SelecteFile = "ENTC";
            scrollPane1.setVisible(false);
            System.out.println("Entc");
            try {
               images = DbHandler.fetchImagesFromDatabaseForBranch(SelecteFile,selectedSemValue);
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            System.out.println("Sem ="+selectedSemValue);
            idbookInfos = DbHandler.getIdbookInfosForBranch(SelecteFile,selectedSemValue);
            ////////////RUNTIME FUNCTIONALITY VISIBILITY;
            comboBox3.getSelectionModel().clearSelection();
            comboBox3.setPromptText("SPPU"); 
            largeScrollPane.setVisible(true);
            createImageViewsAndHBox(primaryStage,largeScrollPane);
            
        });

        Button button4 = new Button("CIVIL");
        button4.setPrefWidth(130);
        button4.setPrefHeight(40);
        button4.setLayoutX(0);
        button4.setLayoutY(82);
         button4.setOnAction(event -> {
            String selectedSemValue = selectedSem.get();
            SelecteFile = "CIVIL";
            scrollPane1.setVisible(false);
            try {
               images = DbHandler.fetchImagesFromDatabaseForBranch(SelecteFile,selectedSemValue);
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            idbookInfos = DbHandler.getIdbookInfosForBranch(SelecteFile,selectedSemValue);
            ////////////RUNTIME FUNCTIONALITY VISIBILITY;
            comboBox3.getSelectionModel().clearSelection();
            comboBox3.setPromptText("SPPU"); 
            largeScrollPane.setVisible(true);
            createImageViewsAndHBox(primaryStage,largeScrollPane);
        });

        Button button5 = new Button("MECHANICAL");
        button5.setPrefWidth(130);
        button5.setPrefHeight(40);
        button5.setLayoutX(0);
        button5.setLayoutY(110);
         button5.setOnAction(event -> {
            String selectedSemValue = selectedSem.get();
            SelecteFile = "MECHANICAL";
            scrollPane1.setVisible(false);
            try {
                images = DbHandler.fetchImagesFromDatabaseForBranch(SelecteFile,selectedSemValue);
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            idbookInfos = DbHandler.getIdbookInfosForBranch(SelecteFile,selectedSemValue);
            ////////////RUNTIME FUNCTIONALITY VISIBILITY;
            comboBox3.getSelectionModel().clearSelection();
            comboBox3.setPromptText("SPPU"); 
            largeScrollPane.setVisible(true);
            createImageViewsAndHBox(primaryStage,largeScrollPane);
        });

        try{

            searchField.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                String searchTerm = searchField.getText().trim();
                System.out.println(searchTerm);
                if (!searchTerm.isEmpty()) {
                    try {
                        DBHandler dbHandler = new DBHandler();
                        if (dbHandler.doesDataExist(searchTerm)){
                            System.out.println("hello Count");
                            images = DbHandler.fetchImagesFromDatabaseForBookName(searchTerm);
                            idbookInfos = DbHandler.getIdbookInfosForBookName(searchTerm);
                            //System.out.println("Number of images in the list: " + images.size());
                            largeScrollPane.setVisible(true);
                            createImageViewsAndHBox(primaryStage,largeScrollPane);
                            
                        } else {
                            System.out.println("No data found");
                            ImageView image = new ImageView(new Image("file:book_revival/Photos/nodatafound.jpg"));
                            image.setLayoutX(90);
                            image.setFitWidth(756);
                            image.setFitHeight(526);
                            Pane imagePane = new Pane(image);
                            largeScrollPane.setContent(imagePane);
                            largeScrollPane.setVisible(true);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        } catch (Exception e) {
            e.printStackTrace();
        }

        ComboBox<String> comboBox4 = new ComboBox<>();
        comboBox4.setPrefWidth(130.0);
        comboBox4.setPrefHeight(40.0);
        comboBox4.setLayoutX(820.0);
        comboBox4.setLayoutY(-2.0);
        comboBox4.setPromptText("MU");
        comboBox4.setCursor(Cursor.HAND);
        comboBox4.getItems().addAll("I","II","III","IV","V","VI","VII","VIII");
         comboBox4.setStyle(
         "-fx-prompt-text-fill: white;" + 
         "-fx-font-size: 20px;" + "-fx-background-color: #0674BE;"
        );

        comboBox4.setOnAction(event -> {
           
            String selectedValue = comboBox4.getValue();
            /////////Have to change this condition when database is sorted
            if (selectedValue != null) {
               scrollPane2.setVisible(true);
            }
            largeScrollPane.setVisible(false);
            dashImg1.setEffect(new GaussianBlur());
            dashImg2.setEffect(new GaussianBlur());
            headlines.setEffect(new GaussianBlur());
            description.setEffect(new GaussianBlur());
        });
       
        ///// all buttons under the MU comboBox4
         Button button1a = new Button("CS");
        button1a.setPrefWidth(130);
        button1a.setPrefHeight(40);
        button1a.setLayoutX(0);
        button1a.setLayoutY(-2);
        button1a.setOnAction(event -> {
            scrollPane2.setVisible(false);
            ////////////RUNTIME FUNCTIONALITY VISIBILITY;      
            comboBox4.getSelectionModel().clearSelection();
            comboBox4.setPromptText("MU");  
             largeScrollPane.setVisible(true);   
        });

        Button button2a = new Button("IT");
        button2a.setPrefWidth(130);
        button2a.setPrefHeight(40);
        button2a.setLayoutX(0);
        button2a.setLayoutY(26);
         button2a.setOnAction(event -> {
            scrollPane2.setVisible(false);
            ////////////RUNTIME FUNCTIONALITY VISIBILITY;      
            comboBox4.getSelectionModel().clearSelection();
            comboBox4.setPromptText("MU");    
             largeScrollPane.setVisible(true); 
        });

         Button button3a = new Button("ENTC");
        button3a.setPrefWidth(130);
        button3a.setPrefHeight(40);
        button3a.setLayoutX(0);
        button3a.setLayoutY(54);
         button3a.setOnAction(event -> {
            scrollPane2.setVisible(false);
            ////////////RUNTIME FUNCTIONALITY VISIBILITY;
            comboBox4.getSelectionModel().clearSelection();
            comboBox4.setPromptText("MU"); 
             largeScrollPane.setVisible(true);
            
        });

        Button button4a = new Button("CIVIL");
        button4a.setPrefWidth(130);
        button4a.setPrefHeight(40);
        button4a.setLayoutX(0);
        button4a.setLayoutY(82);
         button4a.setOnAction(event -> {
            scrollPane2.setVisible(false);
            ////////////RUNTIME FUNCTIONALITY VISIBILITY;
            comboBox4.getSelectionModel().clearSelection();
            comboBox4.setPromptText("MU"); 
             largeScrollPane.setVisible(true);
        });

        Button button5a = new Button("MECHANICS");
        button5a.setPrefWidth(130);
        button5a.setPrefHeight(40);
        button5a.setLayoutX(0);
        button5a.setLayoutY(110);
         button5a.setOnAction(event -> {
            scrollPane2.setVisible(false);
            ////////////RUNTIME FUNCTIONALITY VISIBILITY;
            comboBox4.getSelectionModel().clearSelection();
            comboBox4.setPromptText("MU");
             largeScrollPane.setVisible(true); 
        });



        ComboBox<String> comboBox5 = new ComboBox<>();
        comboBox5.setPrefWidth(130.0);
        comboBox5.setPrefHeight(40.0);
        comboBox5.setLayoutX(1020.0);
        comboBox5.setLayoutY(-2.0);
        comboBox5.setPromptText("BATU");
        comboBox5.setCursor(Cursor.HAND);
        comboBox5.getItems().addAll("I","II","III","IV","V","VI","VII","VIII");
        comboBox5.setStyle(
         "-fx-prompt-text-fill: white;" + 
         "-fx-font-size: 20px;" + "-fx-background-color: #0674BE;"
        );

         comboBox5.setOnAction(event -> {
           
            String selectedValue = comboBox5.getValue();
            /////////Have to change this condition when database is sorted
            if (selectedValue != null) {
               scrollPane3.setVisible(true);
            }
            largeScrollPane.setVisible(false);
            dashImg1.setEffect(new GaussianBlur());
            dashImg2.setEffect(new GaussianBlur());
            headlines.setEffect(new GaussianBlur());
            description.setEffect(new GaussianBlur());

        });



        ///// all buttons under the BATU comboBox5
         Button button1b = new Button("CS");
        button1b.setPrefWidth(130);
        button1b.setPrefHeight(40);
        button1b.setLayoutX(0);
        button1b.setLayoutY(-2);
        button1b.setOnAction(event -> {
            scrollPane3.setVisible(false);
            ////////////RUNTIME FUNCTIONALITY VISIBILITY;      
            comboBox5.getSelectionModel().clearSelection();
            comboBox5.setPromptText("BATU");    
             largeScrollPane.setVisible(true); 
        });

        Button button2b = new Button("IT");
        button2b.setPrefWidth(130);
        button2b.setPrefHeight(40);
        button2b.setLayoutX(0);
        button2b.setLayoutY(26);
         button2b.setOnAction(event -> {
            scrollPane3.setVisible(false);
            ////////////RUNTIME FUNCTIONALITY VISIBILITY;      
            comboBox5.getSelectionModel().clearSelection();
            comboBox5.setPromptText("BATU");   
             largeScrollPane.setVisible(true);  
        });

         Button button3b = new Button("ENTC");
        button3b.setPrefWidth(130);
        button3b.setPrefHeight(40);
        button3b.setLayoutX(0);
        button3b.setLayoutY(54);
         button3b.setOnAction(event -> {
            scrollPane3.setVisible(false);
            ////////////RUNTIME FUNCTIONALITY VISIBILITY;
            comboBox5.getSelectionModel().clearSelection();
            comboBox5.setPromptText("BATU"); 
             largeScrollPane.setVisible(true);
            
        });

        Button button4b = new Button("CIVIL");
        button4b.setPrefWidth(130);
        button4b.setPrefHeight(40);
        button4b.setLayoutX(0);
        button4b.setLayoutY(82);
         button4b.setOnAction(event -> {
            scrollPane3.setVisible(false);
            ////////////RUNTIME FUNCTIONALITY VISIBILITY;
            comboBox5.getSelectionModel().clearSelection();
            comboBox5.setPromptText("BATU"); 
             largeScrollPane.setVisible(true);
        });

        Button button5b = new Button("MECHANICS");
        button5b.setPrefWidth(130);
        button5b.setPrefHeight(40);
        button5b.setLayoutX(0);
        button5b.setLayoutY(110);
         button5b.setOnAction(event -> {
            scrollPane3.setVisible(false);
            ////////////RUNTIME FUNCTIONALITY VISIBILITY;
            comboBox5.getSelectionModel().clearSelection();
            comboBox5.setPromptText("BATU"); 
             largeScrollPane.setVisible(true);
        });

        home_btn.setOnAction(event -> {
            largeScrollPane.setVisible(false);   
            menuPane.setVisible(false);     
            a2.setVisible(false); 
            a1.setVisible(true); 
            b2.setVisible(false); 
            b1.setVisible(true); 
            c2.setVisible(false); 
            c1.setVisible(true); 
            d2.setVisible(false); 
            d1.setVisible(true);
            dashImg1.setEffect(null);
            dashImg2.setEffect(null);
            headlines.setEffect(null);
            description.setEffect(null);
            scrollPane1.setVisible(false);
            scrollPane2.setVisible(false);
            scrollPane3.setVisible(false);
            contactUs_pane.setVisible(false);
        });
        
        menu_btn.setOnAction(event -> {
            menuPane.setVisible(true);  
              dashImg1.setEffect(new GaussianBlur());
              dashImg2.setEffect(new GaussianBlur());
              headlines.setEffect(new GaussianBlur());
              description.setEffect(new GaussianBlur());
          });

          
        content1.getChildren().addAll(button1, button2, button3,button4,button5);
        content2.getChildren().addAll(button1a, button2a, button3a,button4a,button5a);
        content3.getChildren().addAll(button1b, button2b, button3b,button4b,button5b);

        anchorPane1.getChildren().addAll(logoImageView, searchField, searchImageView,c2w_logo);
        // Add the ComboBox to anchorPane2
        anchorPane2.getChildren().addAll(menu_png,menu_btn,home_btn,comboBox3,comboBox4,comboBox5);

        // Add the AnchorPanes to the SplitPane
        splitPane.getItems().addAll(anchorPane1, anchorPane2);

        // Add the SplitPane to the root AnchorPane
        root.getChildren().addAll(dashImg2,dashImg1, headlines,description,largeScrollPane,menuPane,
        splitPane,scrollPane1,scrollPane2,scrollPane3,contactUs_pane);

        // Create a Scene and set it on the primaryStage
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);

        // Set the primaryStage properties
        primaryStage.setTitle("JavaFX Application");
        primaryStage.show();
    }

    public void handleImageViewClick(int id , Stage primaryStage) throws IOException{

            buyBookpage buypage = new buyBookpage();
            buypage.setUser(user);
            Book book = new Book();
    
            DbHandler.buyBookInfo(book,id);
            buypage.setBook(book);
            buypage.start(primaryStage);
    
        }

    public void createImageViewsAndHBox(Stage primaryStage,ScrollPane largeScrollPane){ 
        VBox imagePairsVBox = new VBox(80); 
        System.out.println("Number of images in the list: " + images.size());
       
            if(images.isEmpty()) {
                System.out.println("emptyImagesin Images");

            } else {
                System.out.println("ImagesLoaded");
                HBox currentHBox = new HBox(100); 
                for (int i = 0; i < images.size(); i += 3){

                    int index1 = i;
                    int index2 = i + 1;
                    int index3 = i + 2; 

                    ImageView imageView1 = new ImageView(images.get(index1));
                    ImageView imageView2 = null;
                    ImageView imageView3 = null;

                    imageView1.setFitWidth(250);
                    imageView1.setFitHeight(300);

                    if (i + 1 < images.size()) {
                        imageView2 = new ImageView(images.get(index2));
                        imageView2.setFitWidth(250);
                        imageView2.setFitHeight(300);
                    }

                    if (i + 2 < images.size()) {
                        imageView3 = new ImageView(images.get(index3));
                        imageView3.setFitWidth(250);
                        imageView3.setFitHeight(300);
                    }

                    if (imageView1 != null) {
                        imageView1.setOnMouseClicked(event -> {
                            try {
                                handleImageViewClick(idbookInfos.get(index1),primaryStage);
                            } catch (IOException e) {
                               
                                e.printStackTrace();
                            }
                        });
                    }
                
                    if (imageView2 != null) {
                        imageView2.setOnMouseClicked(event -> {
                            try {
                                handleImageViewClick(idbookInfos.get(index2),primaryStage);
                            } catch (MalformedURLException e) {
                          
                                e.printStackTrace();
                            } catch (IOException e1) {
                             
                                e1.printStackTrace();
                            }
                        });
                    }
                
                    if (imageView3 != null) {
                        imageView3.setOnMouseClicked(event -> {
                            try {
                                handleImageViewClick(idbookInfos.get(index3),primaryStage);
                            } catch (MalformedURLException e) {
                               
                                e.printStackTrace();
                            } catch (IOException e1) {
                
                                e1.printStackTrace();
                            }
                        });
                    }
                    
                    HBox pairHBox = new HBox(100);

                    if (imageView2 != null) {
                        pairHBox.getChildren().add(imageView1);
                        pairHBox.getChildren().add(imageView2);
                    } else if (imageView1 != null) {
                        pairHBox.getChildren().add(imageView1);
                    }

                    if (imageView3 != null) {
                        pairHBox.getChildren().add(imageView3);
                    }

                    currentHBox.getChildren().add(pairHBox);

                    if (i + 3 < images.size()) {
                        imagePairsVBox.getChildren().add(currentHBox);
                        currentHBox = new HBox(100);
                    }
                }

                imagePairsVBox.getChildren().add(currentHBox);
            }
            largeScrollPane.setContent(imagePairsVBox);
        }
        
    public static void main(String[] args) {
        launch(args);
    }
}