package book_revival;

import java.io.File;

public class User {
    private String Username;
    private String Password;
    private String Gender;
    private String Location;
    private String Contact;
    private File image = null;
    
    public void setUsername(String username) {
        Username = username;
    }
    public File getImage() {
        return image;
    }
    public void setImage(File selectedImage) {
        this.image = selectedImage;
    }
    public String getUsername() {
        return Username;
    }
    public String getPassword() {
        return Password;
    }
    public String getGender() {
        return Gender;
    }
    public String getLocation() {
        return Location;
    }
    public String getContact() {
        return Contact;
    }
    public  void setPassword(String password) {
        Password = password;
    } 
    public void setGender(String gender) {
        Gender = gender;
    }
    public void setLocation(String location) {
        Location = location;
    }
    public void setContact(String contact) {
        Contact = contact;
    }

    
    
}
