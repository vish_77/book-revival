
package book_revival;
import java.io.File;
import java.net.MalformedURLException;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class UserInfoPage extends Application {

    DBHandler DbHandler = new DBHandler();
    private User user;
        public void setUser(User user) {
            this.user = user;
        }
    
    @Override
    public void start(Stage primaryStage) {
       
        
        AnchorPane root = new AnchorPane();
        root.getStyleClass().add("bgColor");
        root.getStylesheets().add("book_revival/User.css");

        ImageView imageEdit = new ImageView(new Image("File:book_revival/Photos/icons8-writer-male-94.png"));
        imageEdit.setFitHeight(561);
        imageEdit.setFitWidth(321);
        imageEdit.setLayoutX(75);
        imageEdit.setLayoutY(118);
        imageEdit.setPreserveRatio(true);

        try {
            Image imageEdImage = DbHandler.fetchMyProfileImageFromDatabase(user);
            if (imageEdit != null) {
                imageEdit.setImage(imageEdImage);
            } else {
                imageEdit.setImage(new Image("File:book_revival/photos/icons8-writer-male-94.png"));
            }
        } catch (Exception e) {
            e.printStackTrace();
           
        }

        Button editProfileButton = new Button("Edit Profile");
        editProfileButton.setLayoutX(1150);
        editProfileButton.setLayoutY(46);
        editProfileButton.setMinHeight(30);
        editProfileButton.setPrefWidth(150);
        editProfileButton.setFont(new Font("Arial Black", 12));

        editProfileButton.setOnAction(e->{

           EditProfile edit = new EditProfile();
           edit.setUser(user);
           edit.start(primaryStage); 
        });

        Button backButton = new Button("Back");
        backButton.setLayoutX(19);
        backButton.setLayoutY(584);
        backButton.setMinHeight(20);
        backButton.setPrefWidth(150);
        backButton.setFont(new Font("Arial Black", 21));

         backButton.setOnAction(e->{
            MainPage main = new MainPage();
            main.setUser(user);
            main.start(primaryStage);
        });

        Button logOutButton = new Button("Log Out");
        logOutButton.setLayoutX(1209);
        logOutButton.setLayoutY(591);
        logOutButton.setMinHeight(20);
        logOutButton.setPrefWidth(150);
        logOutButton.setFont(new Font("Arial Black", 17));

        logOutButton.setOnAction(e->{

            loginscean login = new loginscean();
            login.start(primaryStage);
        });

        Label name = new Label("Name:");
        name.setLayoutX(570);
        name.setLayoutY(119);
        name.setPrefHeight(65);
        name.setPrefWidth(178);
        name.setFont(new Font("Arial Black", 40));

        Label password = new Label("Password:");
        password.setLayoutX(570);
        password.setLayoutY(192);
        password.setFont(new Font("Arial Black", 40));

        Label gender = new Label("Gender:");
        gender.setLayoutX(570);
        gender.setLayoutY(267);
        gender.setFont(new Font("Arial Black", 40));

        Label address = new Label("Address:");
        address.setLayoutX(570);
        address.setLayoutY(353);
        address.setFont(new Font("Arial Black", 40));

        Label contactNo = new Label("Contact No:");
        contactNo.setLayoutX(570);
        contactNo.setLayoutY(432);
        contactNo.setFont(new Font("Arial Black", 40));

        Label textLabel2 = new Label(user.getPassword());
        textLabel2.setLayoutX(850);
        textLabel2.setLayoutY(206);
        textLabel2.setPrefHeight(50);
        textLabel2.setPrefWidth(500);
        textLabel2.setFont(new Font(35));

        Label textLabel = new Label(user.getUsername());
        textLabel.setLayoutX(850);
        textLabel.setLayoutY(131);
        textLabel.setPrefHeight(50);
        textLabel.setPrefWidth(500);
        textLabel.setFont(new Font(35));

        Label textLabel3 = new Label(user.getGender());
        textLabel3.setLayoutX(850);
        textLabel3.setLayoutY(281);
        textLabel3.setPrefHeight(50);
        textLabel3.setPrefWidth(500);
        textLabel3.setFont(new Font(35));

        Label textLabel4 = new Label(user.getLocation());
        textLabel4.setLayoutX(850);
        textLabel4.setLayoutY(364);
        textLabel4.setPrefHeight(50);
        textLabel4.setPrefWidth(500);
        textLabel4.setFont(new Font(35));

        Label textLabel5 = new Label(user.getContact());
        textLabel5.setLayoutX(850);
        textLabel5.setLayoutY(443);
        textLabel5.setPrefHeight(50);
        textLabel5.setPrefWidth(500);
        textLabel5.setFont(new Font(35));

        root.getChildren().addAll(
            imageEdit, editProfileButton, backButton, logOutButton,
            name, password, gender, address, contactNo,
            textLabel2, textLabel, textLabel3, textLabel4, textLabel5
        );

        Scene scene = new Scene(root, 1400, 700);
        primaryStage.setScene(scene);
        primaryStage.setTitle("User Profile");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public void setImage(File selectedFile) {
    }
}