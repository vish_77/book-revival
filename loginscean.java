package book_revival;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.geometry.Pos;

public class loginscean extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        AnchorPane root = new AnchorPane();

        ImageView background = new ImageView(new Image(getClass().getResource("/book_revival/Photos/wp12630165-book-pc-wallpapers.jpg").toExternalForm()));
        background.setFitWidth(1400.0);
        background.setFitHeight(700.0);
        background.setLayoutX(14.0);
        background.setEffect(new GaussianBlur());

        TextField usernameField = new TextField();
        usernameField.setFocusTraversable(false);
        usernameField.setLayoutX(882.0);
        usernameField.setLayoutY(267.0);
        usernameField.setPrefWidth(350.0);
        usernameField.setPrefHeight(58.0);
        usernameField.setPromptText("Username");
        usernameField.setStyle("-fx-background-color: #00000;");
        usernameField.setStyle("-fx-background-color: #00000;");
        usernameField.setFont(Font.font("Arial Black", 17)); 

        PasswordField passwordField = new PasswordField();
        passwordField.setFocusTraversable(false);
        passwordField.setLayoutX(882.0);
        passwordField.setLayoutY(373.0);
        passwordField.setPrefWidth(290.0);
        passwordField.setPrefHeight(58.0);
        passwordField.setPromptText("Password");
        passwordField.setStyle("-fx-background-color: #00000;"); 
        passwordField.setFont(Font.font("Arial Black", 17));   

        ToggleButton toggleButton = new ToggleButton();
        toggleButton.setPrefSize(58, 58);
        toggleButton.setLayoutX(1169);
        toggleButton.setLayoutY(373);

        ImageView eyeIcon = new ImageView(new Image("file:book_revival/Photos/eye_Open.png"));
        eyeIcon.setFitHeight(25);
        eyeIcon.setFitWidth(25);

        ImageView eyeSlashIcon = new ImageView(new Image("file:book_revival/Photos/eye_hidden.png"));
        eyeSlashIcon.setFitHeight(25);
        eyeSlashIcon.setFitWidth(25);

        toggleButton.setGraphic(eyeIcon);

        toggleButton.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                passwordField.setVisible(false);
                passwordField.setManaged(false);

                TextField textField = new TextField(passwordField.getText());
                textField.setPrefSize(290, 58);
                textField.setLayoutX(882);
                textField.setLayoutY(373);
                textField.setPromptText("Password");
                textField.setFont(Font.font("Arial Black", 17));
                textField.setAlignment(Pos.CENTER_LEFT);

                root.getChildren().remove(passwordField);
                root.getChildren().add(1, textField);

                toggleButton.setGraphic(eyeSlashIcon);
            } else {
                TextField textField = (TextField) root.getChildren().get(1);
                passwordField.setText(textField.getText());

                root.getChildren().remove(textField);
                root.getChildren().add(1, passwordField);

                passwordField.setVisible(true);
                passwordField.setManaged(true);

                toggleButton.setGraphic(eyeIcon);
            }
        });


        Label signUpLabel = new Label("Don't have an account yet?");
        signUpLabel.setTextFill(Color.WHITE);
        signUpLabel.setLayoutX(890.0);
        signUpLabel.setLayoutY(560.0);
        signUpLabel.setPrefWidth(347.0);
        signUpLabel.setPrefHeight(46.0);
        signUpLabel.setFont(new Font(15.0));
        signUpLabel.setTextAlignment(javafx.scene.text.TextAlignment.JUSTIFY);
        signUpLabel.setFont(Font.font("Microsoft Himalaya", 47.0));

        Button signUpButton = new Button("Sign up");
        signUpButton.setLayoutX(1010.0);
        signUpButton.setLayoutY(615.0);
        signUpButton.setPrefWidth(85.0);
        signUpButton.setPrefHeight(30.0);
        signUpButton.setFont(new Font(18.0));
        signUpButton.setFocusTraversable(false);
        signUpButton.setMnemonicParsing(false);
        signUpButton.setOnAction(event -> signUp_Scene()); 
        signUpButton.setTextOverrun(javafx.scene.control.OverrunStyle.CLIP);
    
        signUpButton.setOnAction(e->{
            SignupApplication signup = new SignupApplication();
            signup.start(primaryStage);
        });
        
        Button loginButton = new Button("Login");
        loginButton.setLayoutX(985.0);
        loginButton.setLayoutY(480.0);
        loginButton.setPrefWidth(142.0);
        loginButton.setPrefHeight(46.0);
        loginButton.setStyle("-fx-background-color: #7B68EE;");
        loginButton.setTextFill(Color.web("#212022"));
        loginButton.setFont(new Font(20.0));

        loginButton.setOnAction(e ->{

            User user = new User();
            user.setUsername(usernameField.getText());
            user.setPassword(passwordField.getText());
            
            user.setUsername(user.getUsername());


            if(user.getUsername().isEmpty() || user.getPassword().isEmpty()){
                Alert alert = new Alert(AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setContentText("All fields are required");
                alert.showAndWait();
            }else{
                DBHandler dbHandler = new DBHandler();
                int x = dbHandler.sinup(user);
                dbHandler.UserInfo(user.getUsername(),user);
                MainPage DashBoard = new MainPage();
                DashBoard.setUser(user);
                if(x != -1) 
                    DashBoard.start(primaryStage);
                else{
                     Alert alert = new Alert(AlertType.ERROR);
                    alert.setHeaderText(null);
                    alert.setContentText("Username and Password is Incorrect");
                    alert.showAndWait();
                }
            }

        });

        Text titleText = new Text("The Book Revival");
        titleText.setLayoutX(150.0);
        titleText.setLayoutY(109.0);
        titleText.setWrappingWidth(1051.30859375);
        titleText.setFont(Font.font("Segoe UI Light", 79.0));
        titleText.setFill(Color.web("#4a0909"));
        titleText.setOpacity(0.83);

        Text sloganText = new Text("Give your books a second life.");
        sloganText.setLayoutX(150.0);
        sloganText.setLayoutY(169.0);
        sloganText.setWrappingWidth(507.822265625);
        sloganText.setFont(Font.font("Microsoft Himalaya", 47.0));
        sloganText.setTextAlignment(javafx.scene.text.TextAlignment.CENTER);

        ImageView logoImage = new ImageView(new Image("book_revival/Photos/Logo.png"));
        logoImage.setLayoutX(32.0);
        logoImage.setLayoutY(600.0);
        logoImage.setFitWidth(118.0);
        logoImage.setFitHeight(105.0);
        logoImage.setOpacity(0.55);

        ImageView userIcon = new ImageView(new Image("book_revival/Photos/icons8-inquiry-skin-type-7-96.png"));
        userIcon.setLayoutX(789.0);
        userIcon.setLayoutY(267.0);
        userIcon.setFitWidth(57.0);
        userIcon.setFitHeight(58.0);

        ImageView passwordIcon = new ImageView(new Image("book_revival/Photos/icons8-password-96.png"));
        passwordIcon.setLayoutX(789.0);
        passwordIcon.setLayoutY(373.0);
        passwordIcon.setFitWidth(57.0);
        passwordIcon.setFitHeight(58.0);

        root.getChildren().addAll(background, usernameField, passwordField, signUpLabel, signUpButton, loginButton,
                titleText, sloganText, logoImage, userIcon, passwordIcon,toggleButton);

        Scene scene = new Scene(root, 1400, 700);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Book Revival App");
        primaryStage.show();
    }

    private Object signUp_Scene() {
        return null;
    }
}
